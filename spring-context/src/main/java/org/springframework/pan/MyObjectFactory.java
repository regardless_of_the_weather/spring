package org.springframework.pan;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Component;

/**
 * @author pyj
 * @date 2022/6/2
 */
@Component
public class MyObjectFactory implements ObjectFactory<ObjectFactoryDemo> {
	@Override
	public ObjectFactoryDemo getObject() throws BeansException {
		return new ObjectFactoryDemo();
	}
}
