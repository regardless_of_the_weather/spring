package org.springframework.pan;

/**
 * @author pyj
 * @date 2022/6/5
 */
public interface AopDemoInterface {

	public void testFactoryBeanDemo();
}
