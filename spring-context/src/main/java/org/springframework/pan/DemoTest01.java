package org.springframework.pan;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author pyj
 * @date 2022/5/22
 */
public class DemoTest01 {

	public static void main(String[] args) {
		//AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(PanConfig.class);
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext("org.springframework.pan");
		for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {

			System.out.println(beanDefinitionName);
		}
		System.out.println("Test AOP");
		PersionInterface bean = applicationContext.getBean(PersionInterface.class);
		bean.logPersion();
	}

}
