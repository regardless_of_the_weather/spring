package org.springframework.pan;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;

/**
 * @author pyj
 * @date 2022/5/22
 */
@Configuration()
@Scope(value = "singleton")
@EnableAspectJAutoProxy
public class PanConfig {

	@Autowired
	private FactoryBeanDemo factoryBeanDemo;

	/*@Bean(autowire = Autowire.NO)
	public Persion person(){
		return new Persion();
	}
*/
}
