package org.springframework.pan;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author pyj
 * @date 2022/6/2
 */
@Component
public class MyFactoryBean implements FactoryBean<FactoryBeanDemo> {

	@Autowired
	private PersionInterface persion;

	/**
	 * 获取bean对象
	 * @return
	 * @throws Exception
	 */
	@Override
	public FactoryBeanDemo getObject() throws Exception {
		return new FactoryBeanDemo();
	}

	/**
	 * 获取对象类型
	 * @return
	 */
	@Override
	public Class<?> getObjectType() {
		return FactoryBeanDemo.class;
	}
}
