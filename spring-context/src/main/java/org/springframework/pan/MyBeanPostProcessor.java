package org.springframework.pan;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @author pyj
 * @date 2022/5/29
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
}
