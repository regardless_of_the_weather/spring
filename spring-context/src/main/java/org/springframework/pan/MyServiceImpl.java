package org.springframework.pan;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pyj
 * @date 2022/6/4
 */
@Service
public class MyServiceImpl implements BeanFactoryAware {

	@Value("张三")
	private String name;

	@Autowired
	private PersionInterface persion;

	@PostConstruct
	public void init(){
		System.out.println("MyServiceImpl.init()");
	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {

	}

	/*public MyServiceImpl(Persion person) {
		this.persion = person;
	}*/
}
