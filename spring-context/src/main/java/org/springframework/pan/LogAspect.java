package org.springframework.pan;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author pyj
 * @date 2022/6/5
 */
@Aspect
@Component
public class LogAspect {

	@Pointcut("execution(* *.logPersion(..))")
	public void webLog(){}

	@Pointcut("execution(* *.testFactoryBeanDemo(..))")
	public void testFactoryBeanDemo(){}

	/**
	 * 环绕通知
	 */
	@Around(value = "webLog()")
	public Object arround(ProceedingJoinPoint pjp) throws Throwable {
		System.out.println("1、Around：方法环绕开始.....");
		Object o =  pjp.proceed();
		System.out.println("3、Around：方法环绕结束，结果是 :" + o);
		return o;

	}

	/**
	 * 环绕通知
	 */
	@After("webLog()")
	public void agter() throws Throwable {
		System.out.println("后置通知");

	}

	/**
	 * 环绕通知
	 */
	@Before("webLog()")
	public void before() throws Throwable {
		System.out.println("前置通知");

	}


}
