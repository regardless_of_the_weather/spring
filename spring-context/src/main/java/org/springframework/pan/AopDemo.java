package org.springframework.pan;

import org.springframework.stereotype.Component;

/**
 * @author pyj
 * @date 2022/6/5
 */
@Component
public class AopDemo implements AopDemoInterface{

	@Override
	public void testFactoryBeanDemo(){}

}
